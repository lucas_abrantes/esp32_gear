//lib wifi correspondente
#include <WiFi.h>
//Libs para configuração da WiFi da ESP
#include <DNSServer.h>
#include <WebServer.h>
#include <WiFiManager.h> 

//Para LED status
#include <Ticker.h>

#define BUILTIN_LED 2

Ticker ticker;

void setup() {
  Serial.begin(115200);
  pinMode(BUILTIN_LED, OUTPUT);
  ticker.attach(1, tick1);

  WiFiManager wifiManager;
  wifiManager.setAPCallback(configModeCallback);
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.startConfigPortal("ESP32_AP");
 
}

void loop() {
  Serial.println(WiFi.SSID());
  delay(10000);
}

void configModeCallback( WiFiManager *myWiFiManager){
  Serial.println("You entered setup mode");
  Serial.println(WiFi.softAPIP());
  Serial.println(myWiFiManager->getConfigPortalSSID());
}

void saveConfigCallback(){
  Serial.println("Configuration saved");
  Serial.println(WiFi.softAPIP());
  ticker.detach();
}

void tick1(){
  //toggle state
  int state = digitalRead(BUILTIN_LED);  // get the current state of GPIO1 pin
  digitalWrite(BUILTIN_LED, !state);     // set pin to the opposite state
}
 
