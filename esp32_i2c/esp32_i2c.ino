//Para o display
#include <Wire.h>  
#include "SSD1306Wire.h"
#include "images.h"


SSD1306Wire  display(0x3c, 5, 4);

//Variáveis globais
float temp_s1 = 20.0;
float humt_s1 = 50.0;
int dataset = 20;

void setup() {
  Serial.begin(115200);
  // Inicializa o objeto que controlará o que será exibido na tela
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_16);

  drawProgressBar(50); 
  drawImage();
  display.display();
 
}

void loop() {
  temp_s1 = random(15, 50);
  humt_s1 = random(50, 101);
  dataset = random(0, 100);

  printDisplay();

  delay(1000);
}

void printDisplay(){
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_16);
  display.drawString(0, 0, "C. A. T.");
  display.setFont(ArialMT_Plain_10);
  display.drawString(0, 15,  "T:");
  display.drawString(20, 15,  String(temp_s1));
  display.drawString(48, 15,  "°C");
  display.drawString(0, 27, "U:");
  display.drawString(20, 27,  String(humt_s1));
  display.drawString(48, 27,  "%");
  display.drawString(0, 40, "Temperatura Desejada");
  display.drawString(0, 50,  String(dataset));
  display.drawString(15, 50 ,  "°C");
  display.display();
}

void drawImage(){
    display.clear();
    display.drawXbm(34, 14, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
}

void drawProgressBar(int t){
  for(int counter=0; counter<=100;counter++){
    display.clear();
    // draw the progress bar
    display.drawProgressBar(0, 32, 120, 10, counter);
    // draw the percentage as String
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 15, String(counter) + "%"); 
    display.display();
    delay(t);
  }
}
